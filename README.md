# zizi's ssh manager

Made for terminal/tty use only

Use a bookmark file so you don't have to remember anything


Tested on
- Artix
- Debian bookworm
- Ubuntu server focal
- XR 14.8 unc0ver

## Dependency
- openssh
- GNU sed
- rsync


## Mode
- remote / local (-r\* / -l\*)

## Syntax example:

**connect**:  ``zsm (mode) connect (box)``

``zsm -l connect pi``: connect to the local raspi

###

**command**:  ``zsm (mode) connect (box) (command)``

``zsm -l command usw2 "cat /etc/ssh/sshd_config"``: tell "usw2" to return output for command "cat /etc/ssh/sshd_config"

###

**send**: ``zsm (mode) send (box) (target) [destination]``

``zsm -r send kor3 main.rs proj1``: remotely send file main.rs to "kor3" at dir /home/user/proj1

###

**get**: ``zsm (mode) get (box) (target) [destination]``

``zsm -r send mex2 /etc/sudoers $HOME/Files/``: remotely get file /etc/sudoers from "mex2" to $HOME/Files

###

**sync**:  ``zsm (mode) sync (to/from) (box) (origin) (destination)``

``zsm -r sync from thonkpad .local/test/zsm test/zsm``: sync dir .local/test/zsm on thonkpad to dir test/zsm on current box

###

## Override

Use an alternative user/key/port variable when connecting

add @user / +key / =port at the end of the command

Example:
- ``zsm -l connect pi @potato`` will connect to "pi" as user "potato"
- ``zsm -r command jp1 +69420 "neofetch"`` will run "neofetch" on "jp1" through port 69420
- ``zsm -l get localbox $HOME/homework =".ssh/localkey"`` will retrieve the "homework" dir from "localbox" using key ".ssh/localkey"

**Yes all overrides can be used in the same command. zsm will recognize them all**

###

## Edit bookmark + add / delete entry

**Edit bookmark**: ``zsm -e`` Yes it uses crontab -e syntax

**Add entry**: ``zsm -a [name] [dhcp (x if remote)] [ip] [port (x if none)] [key (x if none)] [user] [default send destination]``

``zsm -a debian 8 69.420.69.420 69420 /home/zizi/.ssh/sshkey debchan /home/zizi/Files``

**Delete entry**: ``zsm -d [name]``

``zsm -d debian``

## Bookmark example

``art 3 69.420.69.420 3110 /home/zizi/.ssh/sshkey zizi /home/zizi/Files``

Entry "art"
- has dhcp 3 (192.168.1.3)
- outsiders can connect to user "zizi" using IP address "69.420.69.420" on port 3110 and key "/home/zizi/.ssh/sshkey"
- files send to this box will take /home/zizi/Files as default destination


## Install - uninstall

install - ``./install.sh install``

uninstall - ``./install.sh unintsall``

purge - ``./install.sh purge``

install on iOS - ``./install.sh ios``

## Config

All bookmarks and config are in ``$HOME/.local/share/bookmarks/ssh/bookmark``

# TODO
- finish the complettion script
- make zsm use ssh/scp/rsync arguments
    - meaning add ssh-keygen and all the --

#!/bin/sh
# install script for zsm
# mintezpresso

# designed for non root users, but can be used by root too

fn_install () {
    sudo printf "Installing zsm ...\n"
    mkdir -p $HOME/.ssh $HOME/Downloads/ssh $HOME/.config/zsm
    touch $HOME/Downloads/ssh/readme $HOME/.config/zsm/config.sh
    chmod +x $HOME/.config/zsm/config.sh

    [ -z "$(cat $HOME/.config/zsm/config.sh)" ] && printf "# bookmark\nbookmark=\"\$HOME/.ssh/bookmarks\"\n\n# default\nstorage=\"\$HOME/Downloads/ssh\"\n" > $HOME/.config/zsm/config.sh

    [ ! -e "$HOME/.ssh/examplekey" ] && ssh-keygen -f $HOME/.ssh/examplekey -t rsa -b 4096 || printf "Example ssh key pair \"examplekey\" exist. Skipping key creation\n"

    printf "This directory (\$HOME/Downloads/ssh) was made as zsm's default file receiver directory for this machine\nChange default by editing \$HOME/.ssh/bookmarks\n" > $HOME/Downloads/ssh/readme

    [ ! -e "$HOME/.ssh/bookmarks" ] && printf "# syntax: [name] [local dhcp] [remote ip] [key localtion] [user] [default send destination]
# example:
# home $(ip a | grep -wo 192.168.* | cut -c 11) $(curl -s ifconfig.me) $(grep Port /etc/ssh/sshd_config | head -n 1 | awk '{print $NF}') /home/$(whoami)/.ssh/examplekey $(whoami) /home/$(whoami)/Downloads/ssh\n" >> $HOME/.ssh/bookmarks || printf "bookmarks file \$HOME/.ssh/bookmarks exists\n"

    sudo cp zsm /usr/local/bin/zsm
    sudo chmod +x /usr/local/bin/zsm
    printf "Updated zsm\n"

    sudo cp zsm.bash /usr/share/bash-completion/completions/zsm
    printf "Updated zsm bash completion\n"

    printf "Done\n"
}

fn_uninstall () {
    sudo printf "Uninstalling zsm ...\n"
    sudo rm /usr/local/bin/zsm /usr/share/bash-completion/completions/zsm
    printf "Done\n"
}

fn_purge () {
    printf "This operation will remove \$HOME/Downloads/ssh and ssh bookmarks\nAre you sure you want to continue ? (y/N): "
    read yn
    case "$yn" in

        Y | y ) sudo rm /usr/local/bin/zsm /usr/share/bash-completion/completions/zsm
            yes | rm -rf $HOME/Downloads/ssh $HOME/.ssh/bookmarks
            printf "Done\n" ;;

        * ) printf "User cancelled zsm uninstallation\n" && exit ;;
    esac
}

fn_install_ios () {
    sudo printf "Installing zsm ...\n"
    mkdir -p $HOME/.ssh $HOME/Downloads/ssh $HOME/.config/zsm
    touch $HOME/.ssh/bookmarks $HOME/Downloads/ssh/readme $HOME/.config/zsm/config.sh
    chmod +x $HOME/.config/zsm/config.sh

    [ -z "$(cat $HOME/.config/zsm/config.sh)" ] && printf "# bookmark\nbookmark=\"\$HOME/.ssh/bookmarks\"\n\n# default\nstorage=\"\$HOME/Downloads/ssh\"\n" > $HOME/.config/zsm/config.sh

    [ ! -e "$HOME/.ssh/examplekey" ] && ssh-keygen -f $HOME/.ssh/examplekey -t rsa -b 4096 || printf "Example ssh key pair \"examplekey\" exist. Skipping key creation\n"

    printf "This directory (\$HOME/Downloads/ssh) was made as zsm's default file receiver directory for this machine\nChange default by editing \$HOME/bookmarks/ssh/bookmark\n" > $HOME/Downloads/ssh/readme

    printf "# syntax: [name] [local dhcp] [remote ip] [key localtion] [user] [default send destination]
# example:
# phone $(ipconfig getifaddr en0) $(curl ifconfig.me) $(grep Port /etc/ssh/sshd_config | head -n 1 | awk '{print $NF}') /var/mobile/.ssh/examplekey mobile /var/mobile/Media/general_storage\n" >> $HOME/bookmarks/ssh/bookmark

    sed -i 's/^bookmark=.*$/bookmark=\"$HOME\/bookmarks\/ssh\/bookmark\"/' zsm
    sed 's/-O\ //g' zsm

    sudo cp zsm /usr/bin/zsm
    sudo chmod +x /usr/bin/zsm
    printf "Updated zsm\n"

    # we skip the bash completion thing cuz iOS can't use bash for some reasons
    printf "Done\n"
}

case "$1" in
    install ) fn_install ;;
    uninstall ) fn_uninstall ;;
    purge ) fn_purge ;;
    ios ) fn_install_ios ;;
    * ) printf "Available options: install - uninstall - ios - purge\n" ; exit ;;
esac

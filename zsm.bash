#/usr/bin/env bash
# https://opensource.com/article/18/3/creating-bash-completion-script

# INCOMPLETE. DO NOT USE
# PRs welcome

# - hint on bash complete
#     - each zsm session will have its own /tmp/zsmsocket.$pid DIR
#     - completion will be based on arg
#         - completion file be 1 / 2 / 3 / 4 ....
#     - write your shits in there
#     - pid reason cuz we dont want 1 session using shits from another

# goal is for zsm to auto complete args
# and read prev arg before filling

_zsm () {
    local -a opts=(
        -rb
        -rk
        -rp
        -lb
        -lk
        -lp
        -a
        -d
        -e
        -h
        help
    )

    if [[ $2 == * ]]; then
        COMPREPLY=( $(compgen -W "${opts[*]}" - "$2") )
    else
        COMPREPLY=( $(compgen -f -d - "$2") )
    fi
}

complete -F _zsm zsm


    case $prev in
        -l | -r )
            COMPREPLY=($(compgen -W '{0..9}' -- "$cur"))
            return
            ;;
#         -newer | -anewer | -cnewer | -fls | -fprint | -fprint0 | -fprintf | -name | -[il]name | \
#             -ilname | -wholename | -[il]wholename | -samefile)
#             _filedir
#             return
#            ;;
        -fstype)
            _fstypes
            [[ $OSTYPE == *bsd* ]] &&
                COMPREPLY+=($(compgen -W 'local rdonly' -- "$cur"))
            return
            ;;
        -gid)
            _gids
            return
            ;;
        -group)
            COMPREPLY=($(compgen -g -- "$cur" 2>/dev/null))
            return
            ;;
        -xtype | -type)
            COMPREPLY=($(compgen -W 'b c d p f l s' -- "$cur"))
            return
            ;;
        -uid)
            _uids
            return
            ;;
        -user)
            COMPREPLY=($(compgen -u -- "$cur"))
            return
            ;;
        -[acm]min | -[acm]time | -inum | -path | -ipath | -regex | -iregex | -links | -perm | \
            -size | -used | -printf | -context)
            # do nothing, just wait for a parameter to be given
            return
            ;;
        -regextype)
            COMPREPLY=($(compgen -W 'emacs posix-awk posix-basic posix-egrep
                posix-extended' -- "$cur"))
            return
            ;;
    esac
